#!/bin/sh -e

# Define local variables
PG_DB_NAME=seak_leave

PG_DB_USER=$PG_DB_NAME
PG_DB_PASS=$PG_DB_NAME

PG_VERSION=9.4
PG_PORT=15432 

PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"

PG_REPO_APT_SOURCE=/etc/apt/sources.list.d/pgdg.list

PROVISIONED_ON=/etc/vm_provision_on_timestamp

print_help() {
    echo "PostgreSQL v $PG_VERSION has been setup and can be accessed on your local machine ont the forwarded port (default: $PG_PORT)"
    echo "  Host: localhost"
    echo "  Port: $PG_PORT"
    echo "  Database: $PG_DB_NAME"
    echo "  Username: $PG_DB_USER"
    echo "  Password: $PG_DB_PASS"
    echo ""
    echo "psql access to app database user via VM:"
    echo "  vagrant ssh"
    echo "  sudo su - postgres"
    echo "  psql -U $PG_DB_USER $PG_DB_NAME"
    echo ""
    echo "Start hacking:"
    echo "  explain analyze select count(*) from seak_leave;"
}

if [ -f "$PROVISIONED_ON" ]
then
  echo "VM was already provisioned at: $(cat $PROVISIONED_ON)"
  echo ""
  print_help
  exit
fi

if [ ! -f "$PG_REPO_APT_SOURCE" ]
then
  # Add PG apt repo:
  echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" > "$PG_REPO_APT_SOURCE"

  # Add PGDG repo key:
  wget --quiet -O - https://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add -
fi

# Update package list and upgrade all packages
apt-get update
apt-get -y upgrade

# Install postgres
apt-get install -y postgresql-$PG_VERSION postgresql-contrib-$PG_VERSION 

# Edit postgresql.conf to change listen address to '*':
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"

# Configure PostgreSQL to trust everyone:
cat << EOF > /etc/postgresql/$PG_VERSION/main/pg_hba.conf
local all all   trust
host all all all trust
EOF
# Explicitly set default client_encoding
echo "client_encoding = utf8" >> "$PG_CONF"

# Restart so that all new config is loaded:
service postgresql restart

cat << EOF | su - postgres -c psql
-- Create the database user:
CREATE USER $PG_DB_USER WITH PASSWORD '$PG_DB_PASS';

-- Create the database:
CREATE DATABASE $PG_DB_NAME WITH OWNER=$PG_DB_USER
                                  LC_COLLATE='en_US.utf8'
                                  LC_CTYPE='en_US.utf8'
                                  ENCODING='UTF8'
                                  TEMPLATE=template0;
-- Reconnect to created database:
\c $PG_DB_NAME $PG_DB_USER

-- Create seak_leave table:
CREATE TABLE seak_leave(
	empl_id bigserial NOT NULL,
	case_duration integer,
	closed_at timestamp without time zone
);

-- Populate table from sample data
\copy seak_leave from '/vagrant/provision/seak_leave_data.tsv'
EOF

# Tag the provision time:
date > "$PROVISIONED_ON"

echo "Successfully created PostgreSQL dev virtual machine."
echo ""
print_help    
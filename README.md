Requirements
---
- [Vagrant 1.9 +](https://www.vagrantup.com/downloads.html)
- [VirtualBox 5.1 +](https://www.virtualbox.org/wiki/Downloads)

Installation
---

```bash
git clone https://gitlab.com/ecalyari/aggregation-challenge.git
cd ./aggregation-challenge
vagrant up
```